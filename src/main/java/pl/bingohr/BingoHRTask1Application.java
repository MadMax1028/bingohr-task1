package pl.bingohr;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import pl.bingohr.query.Query;
import pl.bingohr.query.QueryExecutor;
import pl.bingohr.scheduling.DailyTaskScheduler;

import java.io.IOException;
import java.time.LocalTime;
import java.util.List;

@SpringBootApplication
public class BingoHRTask1Application {
	public static void main(String[] args) throws IOException {
		ApplicationContext context = SpringApplication.run(BingoHRTask1Application.class, args);
		
		ObjectMapper objectMapper = jacksonObjectMapper();
		DailyTaskScheduler scheduler = context.getBean(DailyTaskScheduler.class);
		QueryExecutor queryExecutor = context.getBean(QueryExecutor.class);
		
		List<QueryScheduleEntry> queryScheduleEntries = objectMapper.readValue(
			BingoHRTask1Application.class.getResourceAsStream("/query-schedule.json"),
			new TypeReference<List<QueryScheduleEntry>>() {}
		);
		
		queryScheduleEntries.forEach(entry -> scheduler.scheduleDailyTask(
			() -> queryExecutor.executeQueries(entry.getQueries()),
			entry.getExecutionTime()
		));
	}
	
	@Bean
	TaskScheduler taskScheduler() {
		return new ThreadPoolTaskScheduler();
	}
	
	private static ObjectMapper jacksonObjectMapper() {
		return new ObjectMapper().findAndRegisterModules().disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
	}
	
	@Getter
	@RequiredArgsConstructor
	private static final class QueryScheduleEntry {
		private final LocalTime executionTime;
		private final List<Query> queries;
	}
}
