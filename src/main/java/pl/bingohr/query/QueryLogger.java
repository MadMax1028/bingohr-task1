package pl.bingohr.query;

import de.vandermeer.asciitable.v2.V2_AsciiTable;
import de.vandermeer.asciitable.v2.render.V2_AsciiTableRenderer;
import de.vandermeer.asciitable.v2.render.WidthLongestLine;
import de.vandermeer.asciitable.v2.themes.V2_E_TableThemes;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.jdbc.support.rowset.SqlRowSetMetaData;
import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
class QueryLogger {
	private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
	
	private final String logSavePath;
	
	QueryLogger(@Value("${bingohr.querylogger.path}") String logSavePath) {
		this.logSavePath = logSavePath;
	}
	
	void logSelectQuery(Query query, SqlRowSet rowSet) {
		if (StringUtils.isNotBlank(query.getResultFile())) {
			SqlRowSetMetaData metadata = rowSet.getMetaData();
			
			V2_AsciiTable asciiTable = new V2_AsciiTable();
			asciiTable.addRule();
			asciiTable.addRow(metadata.getColumnNames());
			asciiTable.addStrongRule();
			
			int columnCount = metadata.getColumnCount();
			while (rowSet.next()) {
				Object[] rowValues = new Object[columnCount];
				for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
					rowValues[columnIndex - 1] = rowSet.getObject(columnIndex);
				}
				asciiTable.addRow(rowValues);
				asciiTable.addRule();
			}
			
			V2_AsciiTableRenderer asciiTableRenderer = new V2_AsciiTableRenderer();
			asciiTableRenderer.setTheme(V2_E_TableThemes.PLAIN_7BIT.get());
			asciiTableRenderer.setWidth(new WidthLongestLine());
			
			saveLog(query.getSql() + "\n" + asciiTableRenderer.render(asciiTable), query.getResultFile());
		}
	}
	
	void logUpdateQuery(Query query, int affectedRows) {
		if (StringUtils.isNotBlank(query.getResultFile())) {
			saveLog(query.getSql() + "\n" + "Affected rows: " + affectedRows, query.getResultFile());
		}
	}
	
	@SneakyThrows
	private void saveLog(String message, String resultFile) {
		String filename = formatter.format(LocalDateTime.now()) + "_" + resultFile + ".txt";
		
		Files.write(new File(logSavePath, filename).toPath(), message.getBytes());
	}
}
