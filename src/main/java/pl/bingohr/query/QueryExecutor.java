package pl.bingohr.query;

import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.regex.Pattern;

@Log
@Component
@RequiredArgsConstructor
public class QueryExecutor {
	private static final Pattern selectQueryPattern = Pattern.compile("^SELECT.*", Pattern.CASE_INSENSITIVE);
	
	private final JdbcTemplate jdbcTemplate;
	private final QueryLogger queryLogger;
	
	@Transactional
	public void executeQueries(Iterable<Query> queries) {
		queries.forEach(query -> {
			logger.info(query.getSql());
			
			if (isSelectQuery(query.getSql())) {
				SqlRowSet rowSet = jdbcTemplate.queryForRowSet(query.getSql());
				queryLogger.logSelectQuery(query, rowSet);
			} else {
				int affectedRows = jdbcTemplate.update(query.getSql());
				queryLogger.logUpdateQuery(query, affectedRows);
			}
		});
	}
	
	private static boolean isSelectQuery(String sql) {
		return selectQueryPattern.matcher(sql).matches();
	}
}
