package pl.bingohr.query;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public final class Query {
	private final String sql;
	private final String resultFile;
}