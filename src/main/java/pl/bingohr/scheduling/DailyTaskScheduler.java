package pl.bingohr.scheduling;

import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@Component
@RequiredArgsConstructor
public class DailyTaskScheduler {
	private static final long ONE_DAY_PERIOD = TimeUnit.DAYS.toMillis(1);
	
	private final TaskScheduler taskScheduler;
	
	public void scheduleDailyTask(Runnable task, LocalTime executionTime) {
		LocalDateTime currentDateTime = LocalDateTime.now();
		
		LocalDate executionDate = currentDateTime.toLocalTime().isAfter(executionTime)
			? currentDateTime.toLocalDate().plusDays(1) // schedule task on the next day
			: currentDateTime.toLocalDate();
		
		Date startTime = Date.from(executionTime.atDate(executionDate).atZone(ZoneId.systemDefault()).toInstant());
		
		taskScheduler.scheduleAtFixedRate(task, startTime, ONE_DAY_PERIOD);
	}
}
