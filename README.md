Zadanie testowe nr 1

Uruchamianie:

Projekt można uruchomić przez dowolne IDE wspierające narzędzie budujące Gradle lub przez wpisanie komendy "gradlew bootRun" w terminalu.

Opis działania aplikacji:

* Przy starcie aplikacji parsowany jest plik w formacie JSON zawierający zapytania do bazy danych oraz godzinę, o której powinny się one wykonać każdego dnia.
* Konfiguracja aplikacji (baza danych, ścieżka zapisu wyników zapytań) jest zdefiniowana w pliku "application.properties".
* Zapytania z jednego zaplanowanego zadania są wykonywane w tej samej tranzakcji.
* Wyniki zapytań są logowane do pliku.
* W przypadku zapytań typu "SELECT" jest tworzona tabela ASCII z wynikami, a w pozostałych jest zapisywana wyłącznie liczba zmodyfikowanych przez zapytanie rekordów.
* To czy dane zapytanie w paczce jest logowane jest również uwzględnione przy definicji zapytań.
* Jeśli godzina planowanego zadania przekracza obecną godzinę to zadanie jest przenoszone na następny dzień na tą samą parę.

Użyte technologie/biblioteki/narzędzia:

* Java 8
* Gradle
* Spring Boot + moduł JDBC
* Jackson
* Apache Commons
* ASCII Table
* Lombok